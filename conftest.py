import pytest


def pytest_runtest_makereport(item, call):
    """ Add _previousfailed attribute if test was failed

    :param item: a basic test invocation item
    :param call: Exception info a function invocation
    """

    parent = item.parent
    if call.when == 'call' and call.excinfo is not None:
        parent._previousfailed = item
    elif call.when == 'call' and call.excinfo is None:
        parent._previousfailed = None


def pytest_runtest_setup(item):
    """ This hook runs before test execution

    if test is marked 'stoponerror' and previous one failed we add _skipall attribute

    :param item: a basic test invocation item
    """

    if 'stoponerror' in item.keywords:
        previousfailed = getattr(item.parent, "_previousfailed", None)
        if previousfailed is not None:
            item.parent._skipall = True


    """ Skip all tests if _skipall attribute exists """
    skip = getattr(item.parent, "_skipall", None)
    if skip is not None:
        pytest.xfail('Skip test')


@pytest.fixture(scope="class")
def preconditions(request):
    """ Set up fixture
    Scope = class, so it executes only once in test class
    """

    test_list = list(range(10))

    def close():
        """ Tear down block
        Here we can clean test environment
        """

        del test_list[:]
    request.addfinalizer(close)

    return test_list