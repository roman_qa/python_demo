import pytest


class TestIt:

    def test_first(self, preconditions):
        assert preconditions[0] == 1

    def test_second(self, preconditions):
        """ To test that it is worked, change current assert to True/False

        True: All test steps in the class should be executed

        False: Only test_first and test_second should be executed, other tests in the class
        should be skipped

        :param preconditions: fixture from conftest.py
        """
        assert preconditions[0] == 1

    @pytest.mark.stoponerror
    def test_third(self, preconditions):
        """ Marked test step that should be skipped if previous one failed. Further tests in the class
        should also be skipped.

        Using @pytest.mark decorator we could mark tests in the way we want.

        :param preconditions: fixture from conftest.py
        """
        assert preconditions[0] == 1

    def test_fourth(self, preconditions):
        assert preconditions[0] == 3